weatherDataFileName = 'weatherData.csv';

%check if weatherData.csv file exists, if not then create this file and
%name columns
if exist(weatherDataFileName, 'file') == 0
    header = ["miejscowosc", "rok", "miesiac", "dzien", "godzina", "godz_ws", "godz_zs", "temp [°C]", "temp_odcz [°C]", "cisnienie [hPa]", "wilgotnosc [%]", "pred_wiatru[km/h]" "różnica sr. temp. dnia i nocy [°C]", "różnica sr. temp. odcz. dnia i nocy [°C]" ];
    writematrix(header, weatherDataFileName, 'WriteMode', 'append');    
end

dayTemperatures = [];
nightTemperatures = [];
dayTemperaturesReal = [];
nightTemperaturesReal = [];
maxTemp = 0;
maxTempHour = 0;
maxRealTemp = 0;
maxRealTempHour = 0;
while 1
    tic %starts measuring time

    %download data from IMGW and wttr.in from three different weather stations
    %and save them in files
    stations_url = ["https://danepubliczne.imgw.pl/api/data/synop/station/katowice", "http://wttr.in/Katowice?format=j1","https://danepubliczne.imgw.pl/api/data/synop/station/czestochowa", "http://wttr.in/Czestochowa?format=j1","https://danepubliczne.imgw.pl/api/data/synop/station/bielskobiala", "http://wttr.in/bielsko-biala?format=j1"];
    filenames = ["katowiceIMGW", "katowiceWTTR", "czestochowaIMGW", "czestochowaWTTR", "bielskobialaIMGW", "bielskobialaWTTR"];
    
    
    disp("#################################");
    
    for i = 1:(length(stations_url))
        filename = "weather" + filenames{i} + ".json";
        websave(filename, stations_url{i}, weboptions('ContentType','json'));
    
    %read data from .json files
    if filenames{i}(end-3:end) == "IMGW"
        str = fileread(filename); 
        data = jsondecode(str);

        temperature = str2double(data.temperatura);
        pressure = str2double(data.cisnienie);
        humidity = str2double(data.wilgotnosc_wzgledna);
        wind_velocity = str2double(data.predkosc_wiatru);
        timeIMGW = str2double(data.godzina_pomiaru);
        dateIMGW = data.data_pomiaru;
        city_str = filenames{i};
        city = city_str(1:end-4);

        
        dateY = dateIMGW(1:4); %year
        dateM = dateIMGW(6:7); %month
        dateD = dateIMGW(9:10); %day
        timeH = timeIMGW; %hour
   
        %calculate real feel temperature (source: https://pl.wikipedia.org/wiki/Temperatura_odczuwalna)
        real_feel_temp = 13.12 + 0.6215 * temperature - 11.37 * wind_velocity^(0.16) + 0.3965 * temperature * wind_velocity^(0.16);

        %looking for maximum temperature of each day
        if maxTemp <= temperature
            maxTemp = temperature;
            maxTempHour = timeH;
        end
        
        if maxRealTemp <= real_feel_temp
            maxRealTemp = real_feel_temp;
            maxRealTempHour = timeH;
        end
        
        %saving data from each measurement in array
        values = [city, dateY, dateM, dateD, timeH, "", "", temperature, real_feel_temp, pressure, humidity, wind_velocity, ""];
    else %when filenames{i}(end-3:end) == "WTTR"
        str = fileread(filename); 
        data = jsondecode(str);
        
        %extracting data about hour of sunrise and sunset from wttr.in json
        %file
        astronomy = data.weather.astronomy;
        sunriseH = str2double(astronomy.sunrise(1:2));
        sunriseM = str2double(astronomy.sunrise(4:5));

        sunsetH = str2double(astronomy.sunset(1:2))+12;
        sunsetM = str2double(astronomy.sunset(4:5));

        values{6}  = astronomy.sunrise(1:2); %hour of sunrise
        values{7}  = astronomy.sunset(1:2);  %hour of sunset
        
        %saving day and night temperatures to arrays
        if timeH >= sunriseH && timeH <= sunsetH
            dayTemperatures(end+1) = temperature;
            dayTemperaturesReal(end+1) = real_feel_temp;
        else
            nightTemperatures(end+1) = temperature;
            nightTemperaturesReal(end+1) = real_feel_temp;
        end
    
        if timeH == 0 && i == length(stations_url)
            tempAverageDiff = mean(dayTempratures) - mean(nightTemperatures);
            realTempAverageDiff = mean(dayTempraturesReal) - mean(nightTemperaturesReal);
            dayTemperatures = [];
            nightTempratures = [];
            dayTemperaturesReal = [];
            nightTemperaturesReal = [];
            values(end-1) = tempAverageDiff;
            values(end-1) = RealTempAverageDiff;
            maxTemp = 0;
            maxTealTemp = 0;
        end
        
        writematrix(values, weatherDataFileName, 'WriteMode', 'append'); 
        disp("Saved data for city " + city);
    end
    
    %calculate difference between average day and night temperatures
    
    end
    
    
    disp("Średnia temperatura w ciągu dnia i nocy = " + values(end));
    disp("Maksymalna temperatura " + dateIMGW + " (dzisiaj) to " + maxTemp + "°C o godz." + maxTempHour);
    disp("Maksymalna temperatura odczuwalna " + dateIMGW + " (dzisiaj) to " + maxRealTemp + "°C o godz." + maxRealTempHour);
    disp("Aktualna temperatura dzisiaj to " + temperature + " °C o godz." + timeH);
    
    elapsedTime = toc; %stops measuring time
    disp("Saved data");
    pause(3600 - elapsedTime); 
    clearvars elapsedTime;
end
